package com.bawei.lib_base_application

import androidx.multidex.MultiDexApplication
import com.squareup.leakcanary.LeakCanary
import com.squareup.leakcanary.RefWatcher

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/1/6
 * Time: 20:17
 * Description: This is BaseApplication
 */
//多模块打包合并时，清单文件下相同属性会产生覆盖
open class BaseApplication : MultiDexApplication() {
    lateinit var ref : RefWatcher

    override fun onCreate() {
        super.onCreate()
        instance = this
        /**
         * 强引用:直接new对象->内部不够抛出oom
         * 软引用:->被软引用标签包裹的对象，内存不够清理内存时会被清理掉
         * 弱引用:->被弱引用标签包裹的对象，一清理内存就会被清理掉
         * 虚引用->清理内存时单独提供一个queue队列用于保存要被清理的对象
         * 使用虚引用进行应用要检测泄露的对象，当清理完成判断队列中对象是否被清理掉
         * 清理掉->正常;为被清理掉->泄露
         * */
        //初始化LeakCanary->如何检测

        ref = LeakCanary.install(this)
    }

    companion object{
        var instance : BaseApplication ?= null
    }
}