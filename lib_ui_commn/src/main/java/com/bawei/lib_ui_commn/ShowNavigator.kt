package com.bawei.lib_ui_commn

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.FragmentManager
import androidx.navigation.NavDestination
import androidx.navigation.NavOptions
import androidx.navigation.Navigator
import androidx.navigation.fragment.FragmentNavigator
import java.util.ArrayDeque

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/1/7
 * Time: 9:53
 * Description: This is ShowNavigator
 */
@Navigator.Name("shownav")
class ShowNavigator constructor(val mContext : Context,val mFragmentManager : FragmentManager,val  mConrainerId : Int)
    :FragmentNavigator(mContext,mFragmentManager,mConrainerId){
    private val mBackStack = ArrayDeque<Int>()

    override fun navigate(
        destination: Destination,
        args: Bundle?,
        navOptions: NavOptions?,
        navigatorExtras: Navigator.Extras?
    ): NavDestination? {
        if (mFragmentManager.isStateSaved) {
            return null
        }
        var className = destination.className
        if (className[0] == '.') {
            className = mContext.packageName + className
        }
        //获取fragment管理器中的fragment，获取到代表当前fragment已经添加
        //获取失败创建fragment对象
        var frag = mFragmentManager.findFragmentByTag(className)
        if (frag == null) {
            return super.navigate(destination, args, navOptions, navigatorExtras)
        }
        return null

    }
}