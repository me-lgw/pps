package com.mooc.ijk.widget

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.activity.ComponentActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.mooc.ijk.widget.media.AndroidMediaController
import com.mooc.ijk.widget.media.IjkVideoView
import com.mooc.lib_network.imgload.ImageLoad
import tv.danmaku.ijk.media.player.IjkMediaPlayer

/**
 * 自定义Framelayout：整合ijkplayer以及covertImag
 * 实现生命周期自动感知
 * */
class IJKView(context: Context, attrs: AttributeSet) : FrameLayout(context, attrs),LifecycleObserver{
    //ijkviewdioview
    //imageview
    private var mMediaController: AndroidMediaController? = null
    var videoView : IjkVideoView
    var convertImg : ImageView
    var activity : ComponentActivity?=null
    init {
        videoView = IjkVideoView(context)
        convertImg = ImageView(context)
        //由于当前自定义view为framelayout所以在动态布局时应
        //先添加ijkVideoView在添加convertImag
        val videoLp = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT)
        videoView.layoutParams = videoLp
        val imgLp = FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
        convertImg.layoutParams = imgLp
        addView(videoView)
        addView(convertImg)
        //初始化activity->view依托于activity级别上下文->context为 activity基类
        //->向下转型获取到appcompatactivity
        if (context is ComponentActivity)
            activity = context
        //生命周期绑定
        if (null != activity) {
            val life = activity!!.lifecycle
            life.addObserver(this)
        }
        initVideoView()
    }

    fun initVideoView(){
        mMediaController = AndroidMediaController(context, false)
        videoView.setMediaController(mMediaController)
    }

    //设置视频封面地址
    fun setConverPath(convertPath: String){
        convertImg.visibility = VISIBLE
        ImageLoad.loadMatch(convertPath,convertImg)
    }

    //设置播放器路径以及封面路径
    fun setPath(videoPath : String){
        videoView.setVideoPath(videoPath)
        convertImg.visibility = GONE
    }

    //校验是否播放
    fun isPlay()=videoView.isPlaying

    //停止播放
    fun relase(){
        videoView.release(true)
    }

    //由于当前IJKView在recyclerview中使用
    //适配器设置数据源进入，但适配器不能调用当前视频播放
    fun startVideo(){
        videoView.start()
        convertImg.visibility = GONE
    }
    @OnLifecycleEvent(value = Lifecycle.Event.ON_PAUSE)
    fun onPause() {
        if (videoView.isPlaying)
            videoView.pause()
    }
    @OnLifecycleEvent(value = Lifecycle.Event.ON_RESUME)
    fun onResume() {
        if (videoView.getmTargetState() == IjkVideoView.STATE_PAUSED)
            videoView.start()
    }
    @OnLifecycleEvent(value = Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        videoView.release(true)
        IjkMediaPlayer.native_profileEnd()
        if (null != activity) {
            val life = activity!!.lifecycle
            life.removeObserver(this)
        }
    }

}