package com.bawei.lib_common

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bawei.lib_base_application.BaseApplication
import com.blankj.utilcode.util.BarUtils
import java.lang.reflect.ParameterizedType

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/1/7
 * Time: 8:16
 * Description: This is BaseActivity
 */


abstract class BaseActivity<VM : ViewModel,V : ViewDataBinding> : AppCompatActivity() {

    protected lateinit var vm: VM
    protected lateinit var v : V

    @SuppressLint("ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        BaseApplication.instance!!.ref.watch(this)
        BarUtils.setStatusBarColor(this,R.color.white)
        v = DataBindingUtil.setContentView(this,bindLayout())
        initView()
        initData()
        initViewModel()
    }

    //创建viewmodel对象
    fun initViewModel() {
        val parameterizedType = this.javaClass.genericSuperclass as ParameterizedType
        val types = parameterizedType.actualTypeArguments
        val cladd : Class<VM> = types[0] as Class<VM>
        vm = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(cladd)
    }

    //初始化控件
    abstract fun initView()

    //创建databinding对象
    abstract fun bindLayout() : Int

    //初始化数据
    abstract fun initData()

    override fun onDestroy() {
        super.onDestroy()
        v.unbind()
    }
}