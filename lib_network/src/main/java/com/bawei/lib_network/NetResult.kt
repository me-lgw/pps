package com.bawei.lib_network

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/1/6
 * Time: 19:49
 * Description: This is NetResult
 */
open class NetResult<out T:Any> {
    data class Sussecc<out T:Any>(val data : T) : NetResult<T>()
    data class Erro(val erromsg : String) : NetResult<Nothing>()
}