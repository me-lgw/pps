package com.bawei.lib_network

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/1/6
 * Time: 19:45
 * Description: This is BaseEntity
 * 数据实体类
 * Retrofit获取json以及使用gson映射返回的实体类
 * T：具体json中的data对应的实体类或者集合（ArrayList）
 */
data class BaseEntity<out T> (val status : Int, val message : String, val data : T) {
}