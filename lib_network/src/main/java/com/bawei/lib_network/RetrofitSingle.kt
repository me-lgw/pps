package com.bawei.lib_network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/1/6
 * Time: 19:50
 * Description: This is RetrofitSingle
 */
class RetrofitSingle private constructor(){
    companion object{
        val retrofitSingle : RetrofitSingle by lazy(mode = LazyThreadSafetyMode.SYNCHRONIZED) {
            RetrofitSingle()
        }
        @Synchronized
        fun instance() : RetrofitSingle{
            return retrofitSingle
        }
    }

    private var retrofit : Retrofit ?= null
    fun getretrofit() : Retrofit{
        if (retrofit == null) {
            createRetrofit()
        }
        return retrofit!!
    }

    fun createRetrofit() {
        val okBuilder = OkHttpClient.Builder()
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        okBuilder.addInterceptor(loggingInterceptor)
            .connectTimeout(30,TimeUnit.SECONDS)
            .readTimeout(30,TimeUnit.SECONDS)
            .writeTimeout(30,TimeUnit.SECONDS)
        val builder = Retrofit.Builder()
        builder.client(okBuilder.build())
            .baseUrl(ApiConst.BASEURL)
            .addConverterFactory(GsonConverterFactory.create())
        retrofit = builder.build()
    }
}