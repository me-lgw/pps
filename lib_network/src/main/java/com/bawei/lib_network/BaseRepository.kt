package com.bawei.lib_network

import kotlinx.coroutines.coroutineScope

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/1/6
 * Time: 19:47
 * Description: This is BaseRepository
 * 全部网络请求基类   相当于MVVM中的model
 */
open class BaseRepository {
    suspend fun <T : Any> requestCall(call : suspend() -> NetResult<T>) : NetResult<T> {
        return call()
    }

    suspend fun <T : Any> handlerResponse(baseEntity: BaseEntity<T>) : NetResult<T> {
        return coroutineScope {
            if (baseEntity.status == 200) {
                NetResult.Sussecc(baseEntity.data)
            } else{
                NetResult.Erro("请求失败")
            }
        }
    }
}